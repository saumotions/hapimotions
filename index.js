var Hapi = require('hapi');

//var server = new Hapi.Server(8000);
var pack = new Hapi.Pack();
pack.server(8000, { labels: ['public'] });

var routes = require('./apps/urls').routes;
console.log(routes);

//server.route(routes);

//server.inject({
        //method: 'GET',
        //url: '/user',
        //simulate: {close:true}
    //}, function(res){
        //console.log("injected");
        //console.log(res.result);
    //}
//);

pack.require('./plugins/news', {}, function(err){
    if (err)
        console.log("error in news plugin");
});

pack.start();

//server.start();
//console.log(server.table());
